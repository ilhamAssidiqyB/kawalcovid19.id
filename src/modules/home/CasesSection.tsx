import * as React from 'react';
import useSWR from 'swr';
import styled from '@emotion/styled';
import format from 'date-fns/format';
import { id } from 'date-fns/locale';

import { Box, Heading, themeProps, Text, Stack } from 'components/ui-core';
import { fetch } from 'utils/api';
import { logEventClick } from 'utils/analytics';

const Link = Text.withComponent('a');

const ReadmoreLink = styled(Link)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.md}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.lg}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.xl} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 4 - 48px), 1fr)
    );
  }
`;

export interface CaseBoxProps {
  color: string;
  data?: Record<string, { value: number }>;
  field: string;
  label: string;
}

const CaseBox: React.FC<CaseBoxProps> = ({ color, data, field, label }) => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="center"
    px="md"
    pt="md"
    pb="lg"
    borderRadius={6}
    backgroundColor="accents01"
  >
    <Box textAlign="center">
      <Text display="block" variant={1100} color={color} fontFamily="monospace">
        {data ? data[field].value : '-----'}
      </Text>
      <Text display="block" mt="xxs" variant={500} color="accents08">
        {label}
      </Text>
    </Box>
  </Box>
);

const CasesSection: React.FC = () => {
  const { data } = useSWR('https://kawalcovid19.harippe.id/api/summary', fetch);

  const formatDate = (dateToFormat: Date) => {
    return format(dateToFormat, 'dd MMMM yyyy HH:mm:ss xxxxx', {
      locale: id,
    });
  };

  return (
    <Stack spacing="xl" mb="xxl">
      <Heading variant={800} as="h2">
        Jumlah Kasus di Indonesia Saat Ini
      </Heading>
      <Box>
        <GridWrapper>
          <CaseBox color="chart" data={data} field="confirmed" label="Terkonfirmasi" />
          <CaseBox color="warning02" data={data} field="activeCare" label="Dalam Perawatan" />
          <CaseBox color="success02" data={data} field="recovered" label="Sembuh" />
          <CaseBox color="error02" data={data} field="deaths" label="Meninggal" />
        </GridWrapper>
        <Box my="md">
          <Text as="h5" m={0} variant={200} color="accents06" fontWeight={400}>
            Pembaruan Terakhir
          </Text>
          <Text as="p" variant={400} color="accents07" fontFamily="monospace">
            {data ? formatDate(new Date(data.metadata.lastUpdatedAt)) : '-----'}
          </Text>
        </Box>
        <ReadmoreLink
          href="https://kcov.id/daftarpositif"
          target="_blank"
          rel="noopener noreferrer"
          color="primary02"
          variant={500}
          onClick={() => logEventClick('Daftar Kasus Lengkap (cases section)')}
        >
          Daftar Kasus Lengkap &rarr;
        </ReadmoreLink>
      </Box>
    </Stack>
  );
};

export default CasesSection;
